unit module Geuse::Pod;
use Pod::To::HTML;

sub make-poder( --> Sub ) {
    sub render-pod( $raw-content ) {
        return Pod::To::HTML.render($raw-content)
    }
    return &render-pod
}

#| Register the Markdown plugin specified in the YAML file.
our sub register( $context is rw, %plugin-config --> Nil ) {
    =begin comment
    - $context is an instance of the GeuseContext class.
    - $plugin-config is a hash that contains the plugin's name and
      the extensions it renders. For instance, %(name => 'Geuse::Markdown', 
      extensions => ['pod6']).
    =end comment

    # add new renderer to content renderer object.
    my @pod-extensions = <pod6>;
    my &poder = make-poder();
    $context.content-renderer.add-renderer(@pod-extensions, &poder);
}
