NAME
====



`Geuse::Pod` - Add Pod rendering capability to the static site generator [Geuse](https://gitlab.com/uzluisf/geuse).

DESCRIPTION
===========



`Geuse::Pod` is only a plugin-wrapper around [`Pod::To::HTML`](https://github.com/perl6/Pod-To-HTML) to facilitate the rendering of Pod-based documents in Geuse.

Therefore, if you want to use Pod as your markup language in Geuse, you must install this module or some other module that provides a similar capability.

For more information on how Geuse picks up the plugin, look at Geuse's [readme page](https://gitlab.com/uzluisf/geuse).

INSTALLATION
============



Using zef:
----------

    zef update && zef install Geuse::Pod

From source:
------------

    git clone git@gitlab.com:uzluisf/geuse-pod.git
    cd geuse-pod && zef install .

DEPENDENCIES
============



This modules depends on [`Pod::To::HTML`](https://github.com/perl6/Pod-To-HTML). If it's not installed on your system, install it locally with

    zef install Pod::To::HTML

AUTHOR
======



Luis F. Uceta

